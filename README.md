# Method of Moments (MoM) for 2D & 1D Scattering and Electrostatics

This project demonstrates application of MoM, also known as the _Moment Method (MM)_ or the _Boundary Element Method (BEM)_, using **_Point Matching_** and **_piecewise constant basis functions_**, to Integral Equations (IE) describing the follwoing problems:
* Scattering from PEC circular cylinder:
  * 2D **_Electric Field Integral Equation_** (EFIE) for TMz polarization  
    _This problem is the numerical equivalent of Equation 11-97 of Ref.[2]_  
  * 2D **_Magnetic Field Integral Equation_** (MFIE) for TEz polarization  
    _This problem is presented in Example 12-6 (and is the numerical equivalent of Equation 11-113) of Ref.[2]_
* Electrostatic charge distribution for a conducting plate  
* Electrostatic charge distribution for a conducting rod  
   _This problem is presented in Example 12-1 of Ref.[2]_  

## References

[1]. 2022. Gibson. The Method of Moments in Electromagnetics. 3rd Edition  
[2]. 2012. Balanis. Advanced Engineering Electromagnetics. 2nd Edition  
[3]. 2009. Chew. Tong. Hu. IE Methods for Electromagnetic and Elastic Waves  

## Code Statistics
<pre>
github.com/AlDanial/cloc v 1.96  T=0.03 s (360.3 files/s, 17942.1 lines/s)
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
Python                           4             33             32            135
MATLAB                           1             14              9            115
PowerShell                       4             28              0             60
Markdown                         1             18              0             54
-------------------------------------------------------------------------------
SUM:                            10             93             41            364
-------------------------------------------------------------------------------
</pre>
## How to Run

* The `YAML` file for creating the conda environment used to run this project is included in `run/conda`
* To run simulations (with experiment `v11` as an example):
  1. `cd` to the main project directory
  2. `.\run\ps1\run_v11.ps1`

## Experiment v11: TMz Wave, Incident on an Infinite PEC Cylinder  

|     |
|:---:|
![][v11_1]

[v11_1]:experiments/v11/data.svg

## Experiment v21: TEz Wave, Incident on an Infinite PEC Cylinder  

|     |
|:---:|
![][v21_1]

[v21_1]:experiments/v21/data.svg

## Experiment v31: Conducting Plate Held at Constant Potential

|     |
|:---:|
![][v31_1]

[v31_1]:experiments/v31/data.svg

## Experiment v41: Conducting Rod Held at Constant Potential

|     |
|:---:|
![][v41_1]

[v41_1]:experiments/v41/data.svg

