#############################################################
### METHOD OF MOMENTS (MOM)                               ###
### POINT MATCHING AND PIECEWISE CONSTANT BASIS FUNCTIONS ###
### MAIN FUNCTION DEFINITIONS                             ###
### by: OAMEED NOAKOASTEEN                                ###
#############################################################

import numpy           as np
import scipy           as sp
import scipy.integrate as integrate

def set_up_the_problem(PATHS,PARAMS):
 if    PARAMS[0][0]==1:                          # TMz POLARIZED WAVE, INCIDENT ON AN INFINITE PEC CYLINDER
  freq                             =300e6        # HERTZ
  k                                =2*np.pi*freq*np.sqrt(PARAMS[1][1]*PARAMS[1][0])
  eta                              =             np.sqrt(PARAMS[1][1]/PARAMS[1][0])
  radius                           =1            # METERS
  angle                            =0            # DEGREES
  E0                               =1
  num_divs                         =100
  config                           =[num_divs,E0,k,eta,radius] 

  integrand                        =lambda phi_i,phi_p: (k*eta/4)*sp.special.hankel2(0,np.sqrt(2)*k*radius*np.sqrt(1-np.cos(phi_p-phi_i)))
  forcing_function                 =lambda phi_i      : E0*np.exp(-1j*k*np.cos((np.pi/180)*angle-phi_i))
  Z                                =np.zeros((num_divs,num_divs), dtype=np.cdouble)
  f                                =np.zeros((num_divs,        ), dtype=np.cdouble)
  
  sub_domains_boundary_points,delta=np.linspace(0, 2*np.pi, num=num_divs+1, endpoint=True, retstep=True)
  sub_domains                      =[(sub_domains_boundary_points[i],sub_domains_boundary_points[i+1]) for i in range(num_divs)]
  observation_points               =[np.mean(sub_domain) for sub_domain in sub_domains ]
  
  for  i,observation_point in enumerate(observation_points):
   f [i  ]                         =forcing_function(observation_point)
   for j,sub_domain        in enumerate(sub_domains       ):
    if j==i:
     Z[i,j]                        =(eta*k/4)*delta*(1+1j*((2/np.pi)*(1+(1/288)*np.power(delta,2)-np.log((1.781*k*radius/4)*delta))))
    else:
     integrand_at_observation_point=lambda phi_p: integrand(observation_point,phi_p)
     Z[i,j], _                     =integrate.quad  (integrand_at_observation_point,sub_domain[0],sub_domain[1], complex_func=True)
 else:
  if   PARAMS[0][0]==2:                          # TEz POLARIZED WAVE, INCIDENT ON AN INFINITE PEC CYLINDER
   freq                             =300e6       # HERTZ
   k                                =2*np.pi*freq*np.sqrt(PARAMS[1][1]*PARAMS[1][0])
   eta                              =             np.sqrt(PARAMS[1][1]/PARAMS[1][0])
   radius                           =1           # METERS
   angle                            =0           # DEGREES
   E0                               =1
   num_divs                         =100
   config                           =[num_divs,E0,k,eta,radius] 
   
   integrand                        =lambda phi_i,phi_p: (1j*k/4)*sp.special.hankel2(1,np.sqrt(2)*k*radius*np.sqrt(1-np.cos(phi_p-phi_i)))*(-1/np.sqrt(2))*np.sqrt(1-np.cos(phi_p-phi_i))
   forcing_function                 =lambda phi_i      : -(1/eta)*E0*np.exp(-1j*k*np.cos((np.pi/180)*angle-phi_i))
   Z                                =np.zeros((num_divs,num_divs), dtype=np.cdouble)
   f                                =np.zeros((num_divs,        ), dtype=np.cdouble)
   
   sub_domains_boundary_points,delta=np.linspace(0, 2*np.pi, num=num_divs+1, endpoint=True, retstep=True)
   sub_domains                      =[(sub_domains_boundary_points[i],sub_domains_boundary_points[i+1]) for i in range(num_divs)]
   observation_points               =[np.mean(sub_domain) for sub_domain in sub_domains ]
  
   for  i,observation_point in enumerate(observation_points):
    f [i  ]                         =forcing_function(observation_point)
    for j,sub_domain        in enumerate(sub_domains       ):
     if j==i:
      Z[i,j]                        =1/2
     else:
      integrand_at_observation_point=lambda phi_p: integrand(observation_point,phi_p)
      Z[i,j], _                     =integrate.quad  (integrand_at_observation_point,sub_domain[0],sub_domain[1], complex_func=True)
  else:
   if  PARAMS[0][0]==3:                          # CONDUCTING PLATE HELD AT CONSTANT POTENTIAL
    length_x                   =1                # METERS
    length_y                   =0.5              # METERS
    num_divs_x                 =10
    V0                         =1

    delta                      =length_x/num_divs_x
    num_divs_y                 =int(length_y/delta)
    config                     =[length_x,num_divs_x,length_y,num_divs_y]
        
    integrand                  =lambda x_i,y_i,x_p,y_p: (1/(4*np.pi*PARAMS[1][0]))*(1/np.sqrt(np.power(x_i-x_p,2)+np.power(y_i-y_p,2)))
    forcing_function           =lambda x_i,y_i        : V0
    Z                          =np.zeros((num_divs_x*num_divs_y,num_divs_x*num_divs_y))
    f                          =np.zeros((num_divs_x*num_divs_y,                     ))

    sub_domains_boundary_points=[np.linspace(0, length_x, num=num_divs_x+1, endpoint=True),
                                 np.linspace(0, length_y, num=num_divs_y+1, endpoint=True) ]
    sub_domains                =[]
    for  i in range(num_divs_x):
     for j in range(num_divs_y):
      sub_domains.append((sub_domains_boundary_points[0][i  ],
                          sub_domains_boundary_points[0][i+1],
                          sub_domains_boundary_points[1][j  ],
                          sub_domains_boundary_points[1][j+1] ))
    observation_points         =[(np.mean(sub_domain[:2]),np.mean(sub_domain[2:])) for sub_domain in sub_domains]

    for  i,observation_point in enumerate(observation_points):
     f  [i  ]                        =forcing_function(*observation_point)
     for j,sub_domain        in enumerate(sub_domains       ):      
      if j==i:
       Z[i,j]                        =(1/(4*np.pi*PARAMS[1][0]))*2*delta*np.log((np.sqrt(2)+1)/(np.sqrt(2)-1))
      else:
       integrand_at_observation_point=lambda x_p,y_p: integrand(*observation_point,x_p,y_p)
       Z[i,j], _                     =integrate.nquad(integrand_at_observation_point, [sub_domain[:2],sub_domain[2:]])
   else:
    if PARAMS[0][0]==4:                          # CONDUCTING ROD HELD AT CONSTANT POTENTIAL
     length                     =1               # METERS
     num_divs                   =100
     h                          =1e-3
     V0                         =1
     config                     =[length,num_divs]

     integrand                  =lambda x_i,x_p: (1/(4*np.pi*PARAMS[1][0]))*(1/np.sqrt(np.power(x_i-x_p,2)+np.power(h,2)))
     forcing_function           =lambda x_i    : V0
     Z                          =np.zeros((num_divs,num_divs))
     f                          =np.zeros((num_divs,        ))
     
     sub_domains_boundary_points=np.linspace(0, length, num=num_divs+1, endpoint=True)
     sub_domains                =[(sub_domains_boundary_points[i],sub_domains_boundary_points[i+1]) for i in range(num_divs)]
     observation_points         =[np.mean(sub_domain) for sub_domain in sub_domains ]
     
     for  i,observation_point in enumerate(observation_points):
      f [i  ]                        =forcing_function(observation_point)
      for j,sub_domain        in enumerate(sub_domains       ):
       integrand_at_observation_point=lambda x_p: integrand(observation_point,x_p)
       Z[i,j], _                     =integrate.quad  (integrand_at_observation_point,sub_domain[0],sub_domain[1])
 return Z, f, np.array(config)

