#############################################################
### METHOD OF MOMENTS (MOM)                               ###
### POINT MATCHING AND PIECEWISE CONSTANT BASIS FUNCTIONS ###
### PARAMETER DEFINITIONS                                 ###
### by: OAMEED NOAKOASTEEN                                ###
#############################################################

import os
import argparse
import numpy    as np

### DEFINE PARSER
parser=argparse.ArgumentParser() 

### DEFINE PARAMETERS
parser.add_argument('-p'   , type=int, help='PROBLEM'           , required=True)
parser.add_argument('-v'   , type=str, help='EXPERIMENT VERSION', required=True)

### ENABLE FLAGS
args=parser.parse_args()

### CONSTRUCT PARAMETER STRUCTURES

eps0= 8.85e-12
mu0 = 4*np.pi*1e-7

PATHS =[os.path.join('..','..','experiments',args.v)]

PARAMS=[[args.p, args.v],
        [eps0  , mu0   ] ]

# PARAMS[0][0]: PROBLEM
# PARAMS[0][1]: EXPERIMENT VERSION
# PARAMS[1][0]: FREE SPACE PERMITTIVITY
# PARAMS[1][1]: FREE SPACE PERMEABILITY

