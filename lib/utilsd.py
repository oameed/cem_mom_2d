#############################################################
### METHOD OF MOMENTS (MOM)                               ###
### POINT MATCHING AND PIECEWISE CONSTANT BASIS FUNCTIONS ###
### UTILITY FUNCTIONS DEFINITIONS                         ###
### by: OAMEED NOAKOASTEEN                                ###
#############################################################

import os
import h5py

def wHDF(FILENAME,DIRNAMES,DATA):
 fobj=h5py.File(FILENAME,'w')
 for i in range(len(DIRNAMES)):
  fobj.create_dataset(DIRNAMES[i],data=DATA[i])
 fobj.close()
 
