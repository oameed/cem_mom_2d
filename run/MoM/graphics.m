%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% METHOD OF MOMENTS (MOM)                               %%%
%%% POINT MATCHING AND PIECEWISE CONSTANT BASIS FUNCTIONS %%%
%%% GRAPHICS                                              %%%
%%% by: OAMEED NOAKOASTEEN                                %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function graphics(PRB,VER, ANALYTIC)
close all
clc

PATHS            ={fullfile('..','..','experiments',VER)}                                       ;
data             =h5read(fullfile(PATHS{1},strcat('data','.h5')), strcat('/','current_density'));
config           =h5read(fullfile(PATHS{1},strcat('data','.h5')), strcat('/','config'         ));
config           =cast(config,"double")                                                         ;

[x   ,x_label   ]=get_independent_variable_for_plot(PRB,config     )                            ;
[data,data_label]=get_data_processed_and_scaled    (PRB,config,data)                            ;

if ANALYTIC
    if PRB==1
        data_analytic=get_analytic_efie(config);
        data         =[data,data_analytic]     ;
    else
        if PRB==2
            data_analytic=get_analytic_mfie(config);
            data         =[data,data_analytic]     ;
        end
    end
end

plotter(PATHS,PRB,data,data_label,x,x_label)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% FUNCTION DEFINITIONS %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function J=get_analytic_mfie(CONFIG)
        N    =25                                                                ;
        A    =-1i*(2*CONFIG(2))/(pi*CONFIG(3)*CONFIG(4)*CONFIG(5))              ;
        PHI  =linspace(0,2*pi,CONFIG(1))                                        ;
        J    =0                                                                 ;
        index=-N:N                                                              ;
        for i=1:size(index,2)
            n=index(i)                                                          ;
            J=J+(2*((1i)^(-n))/(besselh(n-1,2,CONFIG(3)*CONFIG(5))-besselh(n+1,2,CONFIG(3)*CONFIG(5)))).*exp((1i*n).*PHI);
            
        end
        J    =A*J'                                                              ;
        J    =abs(J)/1e-3                                                       ;
    end

    function J=get_analytic_efie(CONFIG)
        N    =25                                                                ;
        A    =(2*CONFIG(2))/(pi*CONFIG(3)*CONFIG(4)*CONFIG(5))                  ;
        PHI  =linspace(0,2*pi,CONFIG(1))                                        ;
        J    =0                                                                 ;
        index=-N:N                                                              ;
        for i=1:size(index,2)
            n=index(i)                                                          ;
            J=J+(((1i)^(-n))/besselh(n,2,CONFIG(3)*CONFIG(5))).*exp((1i*n).*PHI);
        end
        J    =A*J'                                                              ;
        J    =abs(J)/1e-3                                                       ;
    end

    function plotter(PATHS,PRB,DATA,DATA_LABEL,X,X_LABEL,Y,Y_LABEL)
        fig         =figure                                           ;
        fig.Renderer='painters'                                       ;
        position    =fig.Position                                     ;
        fig.Position=[position(1),position(2),position(3),position(3)];
        if ismember(PRB,[1,2])
            plot  (X, DATA(:,1), '-b' , 'linewidth',2)
            hold
            plot  (X, DATA(:,2), '--r', 'linewidth',2)
            xlabel(X_LABEL   )
            ylabel(DATA_LABEL)
            xlim([0 360])
            legend("Numerical", "Analytic")
        else
            if ismember(PRB,[3])
                surf(DATA)
                shading interp 
                axis    equal 
                colormap jet
                set(gca,'Visible' ,'off')
                view(2)
            else
                if ismember(PRB,[4])
                    plot  (X, DATA, '-b', 'linewidth',2)
                    xlabel(X_LABEL   )
                    ylabel(DATA_LABEL)
                end
            end
        end
        saveas(gca,fullfile(PATHS{1},strcat('data','.svg')),'svg')
        close all
    end

    function [DATA,DATA_LABEL]=get_data_processed_and_scaled(PRB,CONFIG,DATA)
        if ismember(PRB,[1,2])
            DATA      =complex(DATA.r,DATA.i)                 ;
            DATA      =abs(DATA)                              ;
            DATA      =DATA./1e-3                             ;
            DATA_LABEL='mA/m'                                 ;
        else
            if ismember(PRB,[3])
                DATA      =reshape(DATA,[CONFIG(4),CONFIG(2)]);
                DATA      =DATA./1e-12                        ;
                DATA_LABEL='pC/m^2'                           ;
            else
                if ismember(PRB,[4])
                    DATA      =DATA./1e-12                    ;
                    DATA_LABEL='pC/m'                         ;
                end
            end
        end
    end

    function [X,X_LABEL]=get_independent_variable_for_plot(PRB,CONFIG)
        if ismember(PRB,[1,2])
            X      =linspace(0,2*pi,CONFIG(1)).*(180/pi)   ;
            X_LABEL='Circumference (degrees)'              ;
        else
            if ismember(PRB,[3])
                X      =[]                                 ;
                X_LABEL=[]                                 ;
            else
                if ismember(PRB,[4])
                    X      =linspace(0,CONFIG(1),CONFIG(2));
                    X_LABEL='Length (m)'                   ;
                end
            end
        end
    end

end

