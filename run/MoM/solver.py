#############################################################
### METHOD OF MOMENTS (MOM)                               ###
### POINT MATCHING AND PIECEWISE CONSTANT BASIS FUNCTIONS ###
### SOLVER                                                ###
### by: OAMEED NOAKOASTEEN                                ###
#############################################################

import os
import sys
import numpy as np
sys.path.append(os.path.join(sys.path[0],'..','..','lib'))
from paramd import PATHS, PARAMS
from maind  import set_up_the_problem
from utilsd import wHDF

Z,f,config=set_up_the_problem(PATHS, PARAMS)
J         =np.linalg.solve   (Z    , f     ) 

wHDF(os.path.join(PATHS[0],'data'+'.h5'),
     ['current_density','config']       ,
     [ J               , config ]        )

