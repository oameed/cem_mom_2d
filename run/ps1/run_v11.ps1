
conda activate pyWin

cd run\MoM

echo ' MAKE DIRECTORY FOR EXPERIMENT v11'

$FileName=                              "..\..\experiments\v11"
if (Test-Path $FileName) {
  rm -Recurse -Force $FileName
}
tar -xzf ..\..\experiments\v00.tar.gz -C ..\..\experiments
mv       ..\..\experiments\v00           ..\..\experiments\v11

echo ' SOLVING 2D EFIE FOR TMz POLARIZED WAVE, INCIDENT ON AN INFINITE PEC CYLINDER '
python solver.py -p 1 -v v11
matlab -batch "graphics(1,'v11',true)" -noFigureWindows

cd ..\..\

conda deactivate pyWin

