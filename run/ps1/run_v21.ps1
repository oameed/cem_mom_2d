
conda activate pyWin

cd run\MoM

echo ' MAKE DIRECTORY FOR EXPERIMENT v21'

$FileName=                              "..\..\experiments\v21"
if (Test-Path $FileName) {
  rm -Recurse -Force $FileName
}
tar -xzf ..\..\experiments\v00.tar.gz -C ..\..\experiments
mv       ..\..\experiments\v00           ..\..\experiments\v21

echo ' SOLVING 2D MFIE FOR TEz POLARIZED WAVE, INCIDENT ON AN INFINITE PEC CYLINDER '
python solver.py -p 2 -v v21
matlab -batch "graphics(2,'v21',true)" -noFigureWindows

cd ..\..\

conda deactivate pyWin

