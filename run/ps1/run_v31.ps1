
conda activate pyWin

cd run\MoM

echo ' MAKE DIRECTORY FOR EXPERIMENT v31'

$FileName=                              "..\..\experiments\v31"
if (Test-Path $FileName) {
  rm -Recurse -Force $FileName
}
tar -xzf ..\..\experiments\v00.tar.gz -C ..\..\experiments
mv       ..\..\experiments\v00           ..\..\experiments\v31

echo ' SOLVING POISSON EQUATION FOR CONDUCTING PLATE HELD AT CONSTANT POTENTIAL '
python solver.py -p 3 -v v31
matlab -batch "graphics(3,'v31',false)" -noFigureWindows

cd ..\..\

conda deactivate pyWin

