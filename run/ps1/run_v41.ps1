
conda activate pyWin

cd run\MoM

echo ' MAKE DIRECTORY FOR EXPERIMENT v41'

$FileName=                              "..\..\experiments\v41"
if (Test-Path $FileName) {
  rm -Recurse -Force $FileName
}
tar -xzf ..\..\experiments\v00.tar.gz -C ..\..\experiments
mv       ..\..\experiments\v00           ..\..\experiments\v41

echo ' SOLVING POISSON EQUATION FOR CONDUCTING ROD HELD AT CONSTANT POTENTIAL '
python solver.py -p 4 -v v41
matlab -batch "graphics(4,'v41',false)" -noFigureWindows

cd ..\..\

conda deactivate pyWin

